//! Types representing inline keyboards.

use crate::types::{callback::Game, LoginUrl};
use is_macro::Is;
use serde::{ser::SerializeMap, Serialize};

/// A shorthand for inline markup.
pub type Markup<'a> = &'a [&'a [Button<'a>]];

/// Represents different types an inline button can be.
///
/// Complete descriptions can be found in [Bots API docs][docs].
///
/// [docs]: https://core.telegram.org/bots/api#inlinekeyboardbutton
#[derive(Debug, PartialEq, Eq, Clone, Hash, Is)]
#[non_exhaustive]
#[must_use]
pub enum ButtonKind<'a> {
    /// Represents a URL button.
    Url(&'a str),
    /// Represents a login button.
    LoginUrl(LoginUrl<'a>),
    /// Represents callback data.
    CallbackData(&'a str),
    /// Represents query inserted when switched to inline.
    SwitchInlineQuery(&'a str),
    /// Represents query inserted when switched to inline in the curent chat.
    SwitchInlineQueryCurrentChat(&'a str),
    /// Represent a description of the game to be laucnhed.
    CallbackGame(Game),
    /// If `true`, a pay button is sent.
    Pay(bool),
}

/// Represents an [`InlineKeyboardButton`].
///
/// [`InlineKeyboardButton`]: https://core.telegram.org/bots/api#inlinekeyboardbutton
#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[must_use]
#[must_use]
pub struct Button<'a> {
    text: &'a str,
    kind: ButtonKind<'a>,
}

/// Represents an [`InlineKeyboardMarkup`].
///
/// [`InlineKeyboardMarkup`]: https://core.telegram.org/bots/api#inlinekeyboardmarkup
#[derive(Debug, PartialEq, Eq, Clone, Hash, Serialize)]
#[must_use]
pub struct Keyboard<'a> {
    inline_keyboard: Markup<'a>,
}

impl<'a> Button<'a> {
    /// Constructs an inline `Button`.
    pub const fn new(text: &'a str, kind: ButtonKind<'a>) -> Self {
        Self { text, kind }
    }
}

impl Serialize for Button<'_> {
    fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
        let mut map = s.serialize_map(Some(2))?;

        map.serialize_entry("text", self.text)?;

        match &self.kind {
            ButtonKind::Url(url) => map.serialize_entry("url", url),
            ButtonKind::LoginUrl(login_url) => {
                map.serialize_entry("login_url", &login_url)
            }
            ButtonKind::CallbackData(callback_data) => {
                map.serialize_entry("callback_data", callback_data)
            }
            ButtonKind::SwitchInlineQuery(query) => {
                map.serialize_entry("switch_inline_query", query)
            }
            ButtonKind::SwitchInlineQueryCurrentChat(query) => {
                map.serialize_entry("switch_inline_query_current_chat", query)
            }
            ButtonKind::CallbackGame(game) => {
                map.serialize_entry("callback_game", &game)
            }
            ButtonKind::Pay(pay) => map.serialize_entry("pay", &pay),
        }?;

        map.end()
    }
}

impl<'a> Keyboard<'a> {
    /// Constructs an inline `Keyboard`.
    pub const fn new(buttons: Markup<'a>) -> Self {
        Self {
            inline_keyboard: buttons,
        }
    }
}

impl<'a> From<Markup<'a>> for Keyboard<'a> {
    fn from(markup: Markup<'a>) -> Self {
        Self::new(markup)
    }
}
