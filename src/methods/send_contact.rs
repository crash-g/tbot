use super::call_method;
use crate::{
    connectors::Client,
    errors, token,
    types::{
        keyboard,
        message::{self, Message},
        parameters::{ChatId, ImplicitChatId, NotificationState},
    },
};
use serde::Serialize;
use std::borrow::Cow;

/// Sends a contact.
///
/// Reflects the [`sendContact`][docs] method.
///
/// [docs]: https://core.telegram.org/bots/api#sendcontact
#[derive(Serialize, Debug, Clone)]
#[must_use = "methods do nothing unless turned into a future"]
pub struct SendContact<'a> {
    #[serde(skip)]
    client: &'a Client,
    #[serde(skip)]
    token: token::Ref<'a>,
    chat_id: ChatId<'a>,
    phone_number: Cow<'a, str>,
    first_name: Cow<'a, str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    last_name: Option<Cow<'a, str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    vcard: Option<Cow<'a, str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    disable_notification: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    reply_to_message_id: Option<message::Id>,
    #[serde(skip_serializing_if = "Option::is_none")]
    reply_markup: Option<keyboard::Any<'a>>,
}

impl<'a> SendContact<'a> {
    pub(crate) fn new(
        client: &'a Client,
        token: token::Ref<'a>,
        chat_id: impl ImplicitChatId<'a>,
        phone_number: impl Into<Cow<'a, str>>,
        first_name: impl Into<Cow<'a, str>>,
    ) -> Self {
        Self {
            client,
            token,
            chat_id: chat_id.into(),
            phone_number: phone_number.into(),
            first_name: first_name.into(),
            last_name: None,
            vcard: None,
            disable_notification: None,
            reply_to_message_id: None,
            reply_markup: None,
        }
    }

    /// Configures the last name of the contact.
    /// Reflects the `last_name` parameter.
    pub fn last_name(mut self, last_name: impl Into<Cow<'a, str>>) -> Self {
        self.last_name = Some(last_name.into());
        self
    }

    /// Configures a VCard for the contact. Reflects the `vcard` parameter.
    #[allow(clippy::doc_markdown)] // no, I don't need to put VCard in backticks
    pub fn vcard(mut self, vcard: impl Into<Cow<'a, str>>) -> Self {
        self.vcard = Some(vcard.into());
        self
    }

    /// Configures if the message will be sent silently.
    /// Reflects the `disable_notification` parameter.
    pub fn notification(mut self, state: NotificationState) -> Self {
        self.disable_notification = Some(state.is_disabled());
        self
    }

    /// Configures which message this contact is sent in reply to.
    /// Reflects the `reply_to_message_id` parameter.
    pub fn reply_to_message_id(mut self, id: message::Id) -> Self {
        self.reply_to_message_id = Some(id);
        self
    }

    /// Configures a keyboard for the message.
    /// Reflects the `reply_markup` parameter.
    pub fn reply_markup(
        mut self,
        markup: impl Into<keyboard::Any<'a>>,
    ) -> Self {
        self.reply_markup = Some(markup.into());
        self
    }
}

impl SendContact<'_> {
    /// Calls the method.
    pub async fn call(self) -> Result<Message, errors::MethodCall> {
        call_method(
            self.client,
            self.token,
            "sendContact",
            None,
            serde_json::to_vec(&self).unwrap(),
        )
        .await
    }
}
