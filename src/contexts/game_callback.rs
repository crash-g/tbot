callback! {
    struct GameCallback {
        /// The requested game.
        game: String,
    } -> EventLoop::game_callback
}
